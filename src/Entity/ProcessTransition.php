<?php

namespace Drupal\process\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\process\ProcessInterface;
use Drupal\process\ProcessTransitionInterface;

/**
 * Defines a process type transition config entity.
 *
 * @ConfigEntityType(
 *   id = "process_transition",
 *   label = @Translation("Process Transition"),
 *   handlers = {},
 *   admin_permission = "administer process transitions",
 *   config_prefix = "process_transition",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "verb",
 *     "process_definition",
 *     "origin",
 *     "destination",
 *     "validators",
 *     "constraints",
 *     "actions",
 *   }
 * )
 */
class ProcessTransition extends ConfigEntityBundleBase implements ProcessTransitionInterface {

  /**
   * The operation plugin that goes with this transition.
   *
   * @var Drupal\operations\OperationInterface
   */
  protected $operation;

  /**
   * The machine name of this process transition.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of this process transition.
   *
   * @var string
   */
  protected $label;

  /**
   * The human-readable verb or verb phrase describing the transition "e.g. Close".
   *
   * @var string
   */
  protected $verb;

  /**
   * The process definition this transition acts on.
   *
   * @var string
   */
  protected $process_definition;

  /**
   * The process status this transition transitions from.
   *
   * @var string
   */
  protected $origin;

  /**
   * The process status this transition transitions to.
   *
   * @var string
   */
  protected $destination;

  /**
   * The validators for this transition operation.
   *
   * This should be a list of rule machine names OR operation_validation
   * plugins if they need to be wrapped in something.
   *
   * @todo Decide between raw rules or validation plugins.
   *
   * @var array
   */
  protected $validators = [];

  /**
   * The constraints for this transition operation.
   *
   * This should be a list of rule machine names OR operation_constraint
   * plugins if they need to be wrapped in something.
   *
   * @todo Decide between raw rules or validation plugins.
   *
   * @var array
   */
  protected $constraints = [];

  /**
   * The actions that should happpen just after this transition has occured.
   *
   * @todo: Potentially this is just a rules event?
   *
   * @ver array
   */
  protected $actions = [];

  /**
   * {@inheritdoc}
   */
  public function operation() {
    if (empty($this->operation)) {
      $this->operation = Drupal::service('plugin.manager.operations')->createInstance('process_transition.'.$this->id);
    }
    return $this->operation;
  }

  /**
   * Allow this entity to be callable.
   */
  public function __invoke(ProcessInterface $process, array $options = []) {
    $op = $this->operation();
    $op($process, $options);
  }
}