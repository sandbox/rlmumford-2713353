<?php

namespace Drupal\process\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\process\ProcessInterface;

/**
 * Process Entity.
 *
 * @ContentEntityType(
 *   id = "process",
 *   label = @Translation("Process"),
 *   label_singular = @Translation("process"),
 *   label_plural = @Translation("processes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count process",
 *     plural = "@count processes"
 *   ),
 *   bundle_label = @Translation("Process Definition"),
 *   handlers = {},
 *   base_table = "process",
 *   data_table = "process_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "definition",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   bundle_entity_type = "process_definition"
 * )
 */
class Process extends ContentEntityBase implements ProcessInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];
    if ($definition = ProcessDefinition::load($bundle)) {
      // Set the target type of the entity field to the entity type that is
      // acted on by this process definition.
      $fields['entity'] = clone $base_field_definitions['entity'];
      $fields['entity']->setSetting('target_type', $definition->getActsOn());
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent'))
      ->setDescription(t('The parent process'))
      ->setSetting('target_type', 'process')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ]);

    $fields['current'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Current'))
      ->setDescription(t('The current sub process'))
      ->setSetting('target_type', 'process')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ]);

    $fields['previous'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Previous'))
      ->setDescription(t('The previous sub process'))
      ->setSetting('target_type', 'process')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ])
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setSetting('max_length', 255);

    $fields['state'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('State'))
      ->setDescription(t('Whether the process is open or closed.'))
      ->setDefaultValue(TRUE);

    $fields['start'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Start time'));

    $fields['end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('End time'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'));

    $fields['entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity'))
      ->setRequired(TRUE);

    return $fields;
  }
}
