<?php

namespace Drupal\process\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\process\ProcessDefinitionInterface;

/**
 * Defines a process type definition config entity.
 *
 * @ConfigEntityType(
 *   id = "process_definition",
 *   label = @Translation("Process Definition"),
 *   handlers = {},
 *   admin_permission = "administer process definitions",
 *   config_prefix = "process_definition",
 *   bundle_of = "process",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "acts_on",
 *     "statuses",
 *   }
 * )
 */
class ProcessDefinition extends ConfigEntityBundleBase implements ProcessDefinitionInterface {

  /**
   * The machine name of this process definition.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of this process definition.
   *
   * @var string
   */
  protected $label;

  /**
   * The entity type this process acts on.
   *
   * @var string
   */
  protected $acts_on;

  /**
   * Whether this definition is the root of a process.
   *
   * @var boolean
   */
  protected $is_root = TRUE;

  /**
   * The statuses that this process definition has.
   *
   * @var array
   */
  protected $statuses;

  /**
   * {@inheritdoc}
   */
  public function isRoot() {
    return $this->is_root;
  }

  /**
   * {@inheritdoc}
   */
  public function getActsOn() {
    return $this->acts_on;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatuses() {
    return $this->statuses;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitions($status) {
    return [];
  }
}