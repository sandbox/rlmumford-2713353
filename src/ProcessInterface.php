<?php

namespace Drupal\process;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for Process Entities.
 */
class ProcessInterface extends ContentEntityInterface {

  /**
   * Close this process.
   *
   * @param boolean $skip_validation
   *   Set this to true to skip the validation of this operation.
   *
   * @return \Drupal\process\ProcessInterface
   *   The process with the operation done (or not done if validation fails).
   *
   * @todo Move this into the operations API?
   */
  public function close($skip_validation = FALSE);

  /**
   * Validate whether or not the process can be closed.
   *
   * @param array &$reasons
   *   An array of reasons why the validation fails.
   *
   * @return boolean
   *   TRUE if the process can be closed, FALSE otherwise.
   */
  public function validateClose(&$reasons = []);

  /**
   * Transition the process using a configured transition.
   *
   * @param string $transition
   *   The transition id.
   * @param boolean $skip_validation
   *   Whether or not to validate before transitioning the process. Defaults to
   *   FALSE.
   * @param ProcessInterface $next_entity
   *   Optionally provide the next child entity.
   */
  public function transition($transition, $skip_validation = FALSE, ProcessInterface $next_entity = NULL);

  /**
   * Validate whether of not this process can transition.
   *
   * This function does not take into account user access.
   *
   * @param string $transition
   *   The machine name of the transition to use.
   * @param array &$reasons
   *   An array of reasons why the validation fails.
   *
   * @return boolean
   *   True is the process can be transitioned, False otherwise.
   */
  public function validateTransition($transition, &$reasons = []);

  /**
   * Undo the last transition that happened.
   *
   * @param boolean $skip_validation
   *   Set this to true to skip the validation of this operation. Defaults to
   *   FALSE.
   */
  public function revert($skip_validation = FALSE);

  /**
   * Determine whether the process can be reverted.
   *
   * @param array &$reasons
   *   An array of reasons why the validation fails.
   *
   * @return boolean
   *   True is the process can be reverted, False otherwise.
   */
  public function validateRevert(&$reasons = []);

  /**
   * Get process definition.
   *
   * @return \Drupal\process\ProcessDefinitionInterface
   */
  public function getDefinition();

  /**
   * Get the previous sub process.
   *
   * @return ProcessInterface|FALSE
   */
  public function getPrevious();

  /**
   * Get the parent process if applicable.
   *
   * @return ProcessInterface|FALSE
   */
  public function getParent();

  /**
   * Set the parent process.
   *
   * @param \Drupal\process\ProcessInterface $process
   *   The process to set as the parent.
   */
  public function setParent(ProcessInterface $process);

  /**
   * Get the current sub process if applicable.
   *
   * @return ProcessInterface|FALSE
   */
  public function getCurrent();

  /**
   * Set the current sub process.
   *
   * @param \Drupal\process\ProcessInterface $process
   *   The process to set as the current.
   */
  public function setCurrent(ProcessInterface $process);

}