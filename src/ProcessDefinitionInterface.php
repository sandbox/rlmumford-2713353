<?php

namespace Drupal\process;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for process definitions.
 */
interface ProcessDefinitionInterface extends ConfigEntityInterface {

  /**
   * Return whether this definition is the root of a process definition.
   *
   * Process statuses can themselves be process definitions. This method gives
   * TRUE if the definition is not used as a status for any other definitions.
   *
   * @return boolean
   */
  public function isRoot();

  /**
   * Get the entity type that the definition acts on.
   *
   * @return string
   *   The entity type id.
   */
  public function getActsOn();

  /**
   * Get the statuses that exist on this definition.
   *
   * @return array
   *   An array of statuses keyed by machine name.
   */
  public function getStatuses();

  /**
   * Get all the transitions for a given status.
   *
   * @param string $status
   *   The status machine name.
   *
   * @return \Drupal\process\ProcessTransitionInterface[]
   *   An array of transition configurations keyed by machine name.
   */
  public function getTransitions($status);
}