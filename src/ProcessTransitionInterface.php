<?php

namespace Drupal\process;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for process definitions.
 */
interface ProcessTransitionInterface extends ConfigEntityInterface {

  /**
   * Get the operation plugin for this transition.
   *
   * @return OperationInterface
   */
  public function operation();

}