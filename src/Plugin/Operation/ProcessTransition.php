<?php

namespace Drupal\process\Plugin\Operation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\operations\OperationBase;

/**
 * Defines the process transition operation behavior.
 *
 * @Operation(
 *  id = "process_transition",
 *  label = @Translation("Transition"),
 *  category = @Translation("Transitions"),
 *  deriver = "Drupal\process\Plugin\Derivative\ProcessTransition"
 * )
 */
class ProcessTransition extends OperationBase {

  /**
   * {@inheritdoc}
   */
  protected function doOperation(EntityInterface $entity, array $options = []) {
    $entity->getCurrent()->close(TRUE)->save();
    $old = $entity->getCurrent();

    if (empty($options['destination_entity'])) {
      if (stripos($this->destination, ':') !== FALSE) {
        list($destination_type, $destination_status) = explode(':', $this->destination, 2);
      }
      else {
        $destination_type = $this->destination;
        $destination_status = NULL;
      }

      $destination_entity = $this->getEntityStorage()->create([
        'definition' => $destination_type,
        'previous' => $old->id,
        'status' => $destination_status,
      ]);
      $destination_entity->setParent($entity);

      $destination_entity->entity = $entity->entity;
      $destination_entity->save();
    }
    else {
      $destination_entity = $options['destination_entity'];
    }

    $entity->setCurrent($destination_entity);
    $entity->save();
  }
}